variable "namespace" {
  type        = "string"
  default     = "sk"
  description = "Namespace, which could be your organization name, e.g. 'pi' (from PlanIt) or 'cp' (from CloudPosse)"
}

variable "stage" {
  type        = "string"
  description = "Stage (environment abbreviation), e.g. 'prd', 'stg', 'dev' or 'tst'"
}

variable "environment" {
  type        = "string"
  description = "Stage, e.g. 'production', 'staging', 'development' or 'testing'"
}

variable "name" {
  type        = "string"
  default     = "foodd"
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "region" {
  type        = "string"
  default     = "us-east-1"
  description = "AWS Region"
}

#----
# VPC
#----

#-----
# VPC
#-----

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
  description = "Availability Zones"
}

variable "private_subnets" {
  type        = "list"
  description = "A list of private subnets inside the VPC"
}

variable "public_subnets" {
  type        = "list"
  description = "A list of public subnets inside the VPC"
}

variable "database_subnets" {
  description = "A list of database subnets"
}

variable "assign_generated_ipv6_cidr_block" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block"
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks	"
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks	"
}

variable "enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires var.azs to be set, and the number of public_subnets created to be greater than or equal to the number of availability zones specified in var.azs."
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
}

variable "cidr" {
  description = "The CIDR block of the VPC"
}

#----
# EC2
#----

variable "instance_type" {
  description = "The type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance."
  default     = "t2.micro"
}
